<?php
// +----------------------------------------------------------------------
// | UrlSplice 地址拼接
// +----------------------------------------------------------------------
// | Copyright (c) 2018 http://www.abc3210.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: 水平凡 <admin@abc3210.com>
// +----------------------------------------------------------------------

namespace zelda;

trait UrlRenewBuildTrait
{
    /**
     * URL地址拼接
     * @param string $originalUrl 原地址
     * @param array $parameter 新加参数
     * @param bool $isOverlay 是否覆盖旧参数
     * @return string
     */
    protected static function urlBuild($originalUrl, array $parameter = [], $isOverlay = false)
    {
        $parseUrl = parse_url($originalUrl);
        $query = [];
        if (isset($parseUrl['query']) && !empty($parseUrl['query'])) {
            parse_str($parseUrl['query'], $query);
        }
        if ($isOverlay) {
            $query = $parameter;
        } else {
            $query = array_merge($query, $parameter);
        }
        $url = '';
        //原本访问路径
        if (isset($parseUrl['scheme'])) {
            $url .= "{$parseUrl['scheme']}://";
        } else {
            //如果是类似  //www.baidu.com/dd/s.html
            if ('//' == substr($originalUrl, 0, 2)) {
                $url .= '//';
            }
        }
        if (isset($parseUrl['host'])) {
            $url .= $parseUrl['host'];
        }
        if (isset($parseUrl['port'])) {
            $url .= ':' . $parseUrl['port'];
        }
        $url .= $parseUrl['path'];
        if (!empty($query)) {
            $url .= '?';
            $url .= http_build_query($query);
        }
        //描点
        if (isset($parseUrl['fragment'])) {
            $url .= "#{$parseUrl['fragment']}";
        }
        return $url;
    }
}