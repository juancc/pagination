<html>
<head>
    <style type="text/css">
        div.page {
            clear: both;
        }

        ul {
            clear: both;
            height: 31px;
        }

        li {
            float: left;
            display: block;
            padding: 5px;
        }

        li span, li span:active, li span:visited, li a {
            color: #000;
        }

        li span.current {
            color: red;
        }
    </style>
</head>
<body>

</body>
</html>
<?php

use zelda\Pagination;

require_once '../vendor/autoload.php';
echo '示例1：最简代码：<br/>';
$config = [
    'pageParam' => 'p',
    'currentPage' => $_GET['p'] ? (int)$_GET['p'] : 1,
    'totalNum' => 340,
];
$pagination = Pagination::instance($config);
$html = $pagination->create();
echo $html;
exit;
//echo '<br/>';
echo '示例1：自定义配置参数代码：<br/>';
$pagination = new \zelda\Pagination([
    'baseUrl' => '/test/PaginationTest.php||/test/PaginationTest.php?page=#page#',//链接规则
    'totalNum' => 109,
    'currentPage' => $_GET['page'] ? (int)$_GET['page'] : 1,
    'isShowRegion' => false,
    'containerTagOpen' => '<div>',
    'containerTagClose' => '</div>',
]);
$pagination->setAttr('baseUrl', '/test/PaginationTest.php||/test/PaginationTest.php?page=#page#')
    ->setAttr('totalNum', 101)
    ->setAttr('currentPage', $_GET['page'] ? (int)$_GET['page'] : 1)
    ->setAttr('isShowRegion', true)
    ->setAttr('containerTagClass', 'page')
    ->setAttr('containerTagOpen', '<div>')
    ->setAttr('containerTagClose', '</div>')
    ->setAttr('itemTagOpen', '')
    ->setAttr('itemTagClose', '');
$html = $pagination->create();
echo $html;
exit;
$txt = $pagination->create();

echo $txt;
