# composer分页类

#### 项目介绍
一个composer分页插件
改编自作者ustb80的代码
支持自定义模板规则

#### 使用说明

1.、最简调用（提供一个当前页码，数据总数）
```
$config = [
    'currentPage' => $_GET['page'] ? (int)$_GET['page'] : 1,
    'totalNum' => 999
];
$pagination = Pagination::instance($config)->create();
```

输出：
```
<ul>
  <li class="all"><span>共有999条信息</span></li>
  <li><span>当前第1页</span></li>
  <li><span class="current">1</span></li>
  <li><a href="http://p.com/test/PaginationTest.php?page=2">2</a></li>
  <li><a href="http://p.com/test/PaginationTest.php?page=3">3</a></li>
  <li><a href="http://p.com/test/PaginationTest.php?page=4">4</a></li>
  <li><a href="http://p.com/test/PaginationTest.php?page=5">5</a></li>
  <li>...</li>
  <li><a href="http://p.com/test/PaginationTest.php?page=100">100</a></li>
  <li><a href="http://p.com/test/PaginationTest.php?page=2">下一页</a></li>
  <li><a href="http://p.com/test/PaginationTest.php?page=100">尾页</a></li>
  <li><input type="text" size="5" id="__jumpTo" value="1" onkeydown="return pageJump(event);"/></li>
</ul>
```
2、 自定义结构样式
```
$pagination = Pagination::instance()
    ->setAttr('currentPage', $_GET['page'] ? (int)$_GET['page'] : 1)
    ->setAttr('totalNum', '999')
    ->setAttr('containerTagOpen', '<div>')
    ->setAttr('containerTagClose', '</div>')
    ->setAttr('itemTagOpen', '')
    ->setAttr('itemTagCclose', '')
    ->setAttr('containerTagClass','page')
    ->setAttr('baseUrl','/test/PaginationTest.php||/test/PaginationTest.php?page=#page#')
    ->create();
print_r($pagination);
```
输出：
```php
<div class="page"><span>共有999条信息</span>
<span>当前第1页</span>
<span class="current">1</span>
<a href="http://p.com/test/PaginationTest.php?page=2">2</a>
<a href="http://p.com/test/PaginationTest.php?page=3">3</a>
<a href="http://p.com/test/PaginationTest.php?page=4">4</a>
<a href="http://p.com/test/PaginationTest.php?page=5">5</a>
...
<a href="http://p.com/test/PaginationTest.php?page=100">100</a>
<a href="http://p.com/test/PaginationTest.php?page=2">下一页</a>
<a href="http://p.com/test/PaginationTest.php?page=100">尾页</a>
<input type="text" size="5" id="__jumpTo" value="1" onkeydown="return pageJump(event);"/>
</div>
```
3、 更多参数请看 Pagination.php 参数说明

